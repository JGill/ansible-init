## Ansible-init
Ansible playbook to quickly set up a PC, install/upgrade software packages.
Including: htop, nano, wget, zsh, quickz-sh, atom, docker, python3-pip, rclone, restic, keepassxc, nextcloud-client, croc etc.   

#### Usage

1. Clone repo: `git clone https://gitlab.com/JGill/ansible-init`
2. Create "hosts" file to specify hosts (to perform these installs on)

example "hosts" file
```bash
[local-pc]
localhost   ansible_connection=local normal_user=john

[webserver]
yourwebserver.com   normal_user=webadmin

[another-pc]
10.0.0.10     # using ip

[all:vars]       # Note: all seems to be special word
ansible_python_interpreter=/usr/bin/python3
ansible_user=root

```

3. Uncomment the "tasks" to perform in "init.yml".

4. Run it while providing host and it's "normal_user" variable. normal_user is the logged in user if it is not root.
`sudo ansible-playbook init.yml -v -i hosts --limit 'local-pc' -e 'normal_user=john'`
